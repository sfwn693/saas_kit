import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios"
import styled from "styled-components";
import eclipse from "../../assets/images/Ellipse 2.png"
import BgImage from "../../assets/images/Bg.png"
import Message from "../../assets/images/codicon_mail.png"
import lock from "../../assets/images/padlock (1) 2.png"
import {Helmet} from "react-helmet"
import { UserContext } from "../../App";

export default function Login() {
    const [username,setUsername] = useState("");
	const [password,setPassword] = useState("");
    const [message,setMessage] = useState("");


    const history = useHistory();

    const handleSubmit= (e) => {
        e.preventDefault();
        axios
            .post(`http://saaskit.tegain.com/auth/jwt/create/`, { username, password})
            .then((response) => {
                let data = response.data;
                localStorage.setItem("user_data",JSON.stringify(data));
                console.log(data)
                history.push("/");
            }).catch((error) => {
                if(error.response.status === 401){
                    setMessage("User not exist.") 
                }
            })
    }

    // useEffect(() => {
    //     let isAuth = localStorage.getItem('user_data')
    //     if(isAuth & isAuth !== 'undefined') {
    //        history.push('/')
    //     }
    //  }, [])
 
    return (
        <>
            <Helmet>
                <title>Login</title>
            </Helmet> 
            <Body>
                <LeftContainer>
                    <Content>
                        <ContentHeading>GoFinance</ContentHeading>
                        <ContentParagraph>The most popular peer to peer lending at SEA</ContentParagraph>
                        <ContentButton>Read More</ContentButton>
                    </Content>
                    <EclipsContainer>
                        <EclipsImage src={eclipse} alt="Image" />
                    </EclipsContainer>
                    <EclipsContainer2>
                        <EclipsImage2 src={eclipse} alt="Image" />
                    </EclipsContainer2>
                </LeftContainer>
                <RightContainer>
                    <Heading>SaaS Kit</Heading>
                    <SubTitle>Hello!</SubTitle>
                    <Paragrapgh>Sign Up to Get Started</Paragrapgh>
                    <FormContainer onSubmit={handleSubmit}>
                        <EmailContainer>
                            <ImgContainer>
                                <Image src={Message} alt="message" />
                            </ImgContainer>
                            <EmailInput onChange={(e) => setUsername(e.target.value)} value={username} type="text" placeholder="Email Address" />
                        </EmailContainer>
                        <EmailContainer>
                            <ImgContainer>
                                <Image src={lock} alt="lock" />
                            </ImgContainer>
                            <EmailInput onChange={(e) => setPassword(e.target.value)} type="password" value={password}  placeholder="Password" />
                        </EmailContainer>
                        {message && <ErrorMessage>{message}</ErrorMessage>} 
                        
                        <SubmitButton type="submit" value="Login"/>
                    </FormContainer>
                </RightContainer>
            </Body>
        </>
    )
}


const Body = styled.section`
    display: flex;
    /* justify-content:space-between; */
    overflow: hidden;
    height: 100vh;
`;
const LeftContainer = styled.div`
    width: 60%;
    background: url(${BgImage});
    background-size: cover;
    /* height: 100vh; */
`;
const Content = styled.div`
  width: 50%;
  margin: 0 auto;
  margin-top: 250px;
  margin-left: 150px;
`;
const ContentHeading = styled.h2`
  color: #fff;
  font-size: 40px;
  margin: 0px;
  margin-bottom: 15px;
`;
const ContentParagraph = styled.p`
  font-size: 15px;
  color: #fff;
  font-weight: 600;
  margin-bottom: 15px;
`;
const ContentButton = styled.button`
  border: none;
  background: #0575e6;
  color: #fff;
  padding: 10px 30px;
  border-radius: 8px;
`;
const EclipsContainer = styled.div`
  width: 27%;
  position: relative;
`;
const EclipsImage = styled.img`
  position: absolute;
  top: 50px;
  right: -50px;
`;
const EclipsContainer2 = styled.div`
  width: 31%;
  position: relative;
`;
const EclipsImage2 = styled.img`
  position: absolute;
  top: 80px;
`;

const RightContainer = styled.div`
    padding: 50px 150px 100px 140px;
`;
const Heading = styled.h2`
    font-size: 40px;
    color: #0575e6;
    margin-bottom: 50px;
`;
const SubTitle = styled.h5`
    font-size: 32px;
    font-weight: 700;
    margin: 0px;
`;
const Paragrapgh = styled.p`
    margin-top: 0px;
    font-size: 20px;
`;
const FormContainer = styled.form`
    display: flex;
    justify-content: center;
    flex-direction: column;
`;
const EmailContainer = styled.div`
    display: flex;
    padding:10px 5px;
    background: #fff;
    border: 1px solid #C2CFE0;
    margin: 10px auto;
    border-radius: 4px;
`;
const ImgContainer = styled.div`
    width: 15%;
    margin-right: 5px;
`;
const Image = styled.img`
    display: block;
    width: 100%;
`;
const EmailInput = styled.input`
    border: none;
`;
const SubmitButton = styled.input`
    width:100%;
    margin: 0 auto;
    padding: 15px 40px;
    background: #0575e6;
    border: none;
    color: #fff;
    cursor: pointer;
    border-radius: 4px;
` ;
const ErrorMessage = styled.p`
    font-size: 17px;
    color: red;
    margin-bottom: 25px;
    text-align: center;
`;