import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import axios from "axios";
import { useState, useContext, useEffect } from "react";
import { type } from '@testing-library/user-event/dist/type';


ChartJS.register(ArcElement, Tooltip, Legend);


export default function DemoGraph() {
    // const[users,setUsers] = useState([]);
    const[labels,setLabels]= useState([]);
    const[graph_data,setGraph_data]= useState([]);

    const api = '  http://saaskit.tegain.com/api/dashboard/pie-chart/'; 
    const token = JSON.parse(localStorage.getItem("user_data")).data.access;

    useEffect(() =>{
        axios.get(api, { headers: {"Authorization" : `Bearer ${token}`} })
        .then(function (response) {
            // handle success
            console.log(response.data.data);
            // setUsers(response.data.data);
            setLabels(Object.keys(response.data.data))
            setGraph_data(Object.values(response.data.data))
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }, [])
    const datas = {
        labels: labels,
        datasets: [
          {
            label: '# of Votes',
            data: graph_data,
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
            ],
            borderWidth: 1,
          },
        ],
      };
      console.log(datas.labels);
      datas.datasets.forEach(data =>{
        console.log(data.data)
      })

      
    
    // console.log(users)
    // console.log(labels)
    // console.log(graph_data)
    


  return <Pie data={datas} />;
}

