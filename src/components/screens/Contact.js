import React from 'react';
import axios from "axios";
import Nav from '../includes/Nav';
import { useState, useContext, useEffect } from "react";
import {Helmet} from "react-helmet"
import { Link, useHistory } from "react-router-dom";



export default function Contact() {
    const[users,setUsers] = useState([]);
    const api = ' http://saaskit.tegain.com/api/contact/'; 
    const token = JSON.parse(localStorage.getItem("user_data")).data.access;
    // console.log(token)

    useEffect(() =>{
        axios.get(api, { headers: {"Authorization" : `Bearer ${token}`} })
        .then(function (response) {
            // handle success
            setUsers(response.data.data.results);
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    })

    let renderItems= () =>{
        return users.map((user) =>(
            <tr>
                <td><input type="checkbox" /></td>
                <Link to={`/user/${user.id}`}><td><p className='name'>{user.name}</p></td></Link>
                <td><p>{user.email}</p></td>
                <td><p>{user.company_name}</p></td>
                <td><p>{user.role}</p></td>
                <td><p>{user.forecast}</p></td>
                <td><p>{user.recent_activity_naturaltime}</p></td>
            </tr>
        ))
    }

    return (
        <>
            <Nav/>
            <Helmet>
                <title>Contact</title>
            </Helmet>
            <section id="contact">
                <div className='list-items'>
                    <table>
                        <thead>
                            <tr>
                                <th><input type="checkbox" /></th>
                                <th><p className='title'>Name</p></th>
                                <th><p className='title'>Email</p></th>
                                <th><p className='title'>Company Name</p></th>
                                <th><p className='title'>Role</p></th>
                                <th><p className='title'>Forecast</p></th>
                                <th><p className='title'>Recent activity</p></th>
                            </tr>  
                        </thead>
                        <tbody>
                            {renderItems()}
                        </tbody>
                    </table>           
                </div>
            </section>
        </>
    )
}
