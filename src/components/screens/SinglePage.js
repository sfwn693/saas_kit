import React,{useState, useEffect} from 'react'
import axios from 'axios';
import { useParams } from 'react-router';
import {Helmet} from "react-helmet"


export default function SinglePage() {
    const [user,setUser] = useState([]);
    const {id} = useParams(); //we got that value in id.
    const token = JSON.parse(localStorage.getItem("user_data")).data.access;
    useEffect(() => {
        // backtick
        axios.get(`http://saaskit.tegain.com/api/contact/${id}`, { headers: {"Authorization" : `Bearer ${token}`} }) 
        // use useparams hook to pass id as dynamic.ie instead of 2 in abovr url.
        .then(function (response) {
            // handle success
            console.log(response.data)
            setUser(response.data.data);
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }, [])
    
    return (
        <div>
            <Helmet>
                <title>{`${user.name}`}</title>
            </Helmet>
            <h1>{user.name}</h1>
        </div>
    )
}