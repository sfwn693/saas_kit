import React, { useState, useContext } from "react";
import { UserContext } from "../../App";
import { BrowserRouter as Router, Route, Switch,Redirect } from "react-router-dom"

export default function PrivateRoute({component: Component, ...rest}) {
    const isLoggin = useContext(UserContext)
    console.log(isLoggin)

    return (
        <Route {...rest}
            render={(props) => {
                if(isLoggin){
                    return <Component {...props}/>
                }
                else{
                    return <Redirect to={{pathname:"/login"}} />
                }
            }}
        />
    )
}