import React from 'react'
import Nav from '../includes/Nav'
import Userpic from "../../assets/images/user.png"
import DemoGraph from './DemoGraph'
import {Helmet} from "react-helmet"


export default function Dashboard() {
    return (
        <section id="dashboard">
            <Nav />
            {/* <Helmet>
                <title>Dashboard</title>
            </Helmet> */}
            <section id="dashboard-section">
                <div class="left">
                    <div class="top">
                        <p class="task">8 task completed out of 10</p>
                        <p class="show">show:<b>This Week</b></p>
                    </div>
                    <div class="border">
                        <div class="green">

                        </div>
                    </div>
                    <p class="datee">
                        23 December, Sunday
                    </p>
                    <ul class="calender">
                        <li>
                            <p class="day">
                                Sun
                            </p>
                            <p class="date">
                                24
                            </p>
                        </li>
                        <li>
                            <p class="day">
                                Mon
                            </p>
                            <p class="date">
                                25
                            </p>
                        </li>
                        <li>
                            <p class="day">
                                Tue
                            </p>
                            <p class="date">
                                26
                            </p>
                        </li>
                        <li>
                            <p class="day">
                                Wed
                            </p>
                            <p class="date">
                                27
                            </p>
                        </li>
                        <li>
                            <p class="day">
                                Thu
                            </p>
                            <p class="date">
                                28
                            </p>
                        </li>
                        <li>
                            <p class="day">
                                Fri
                            </p>
                            <p class="date">
                                29
                            </p>
                        </li>
                        <li>
                            <p class="day">
                                Sat
                            </p>
                            <p class="date">
                                30
                            </p>
                        </li>
                    </ul>
                    <ul class="task">
                        <li>
                            <div class="top">
                                <p class="day">Send benefit review by sunday</p>
                                <p class="reminder">Reminder</p>
                            </div>
                            <p class="date">
                                Due date:December 23 2018
                            </p>
                            <div class="user">
                                <div class="man">
                                    <img src={Userpic} alt="d" />
                                </div>
                                <p class="name">
                                    George Fields
                                </p>
                                <div class="complete">
                                    Completed
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="top">
                                <p class="day">Invite to office meetup</p>
                                <p class="reminder">call</p>
                            </div>
                            <p class="date">
                                Due date:December 23 2018
                            </p>
                            <div class="user">
                                <div class="man">
                                    <img src={Userpic} alt="d" />
                                </div>
                                <p class="name">
                                    Rebecca moore
                                </p>
                                <div class="end">
                                    Ended
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="top">
                                <p class="day">Office meetup</p>
                                <p class="reminder">Event</p>
                            </div>
                            <p class="date">
                                Due date:December 23 2018
                            </p>
                            <div class="user">
                                <div class="man">
                                    <img src={Userpic} alt="d" />
                                </div>
                                <p class="name">
                                Lindsey Stroud
                                </p>
                                <div class="complete">
                                    Completed
                                </div>
                            </div>
                        </li>
                    </ul>
                    <a class="footer">
                        Show more
                    </a>
                </div>
            </section>
            <section className='right-pie'>
                <DemoGraph />
            </section>
        </section>
    )
}
