import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
import React, {useState, useEffect,useContext} from "react";
import Login from "./components/screens/Login";
import Dashboard from './components/screens/Dashboard';
import PrivateRoute from './components/screens/PrivateRoute';
import Contact from './components/screens/Contact';
import SinglePage from './components/screens/SinglePage';


export const UserContext = React.createContext();

function App(props) {
  const [userData, setUserData] = useState({});

  useEffect(() => {
    setUserData(JSON.parse(localStorage.getItem("user_data")));
  }, [])

  return (
    <UserContext.Provider value={userData}>
      <Router>
          <PrivateRoute path="/" exact component={Dashboard} />
          <Route path="/login" component={Login} />
          <PrivateRoute path="/contact" component={Contact} />
          <Route path="/user/:id" component={SinglePage} />  
      </Router>
    </UserContext.Provider>
  );
}

export default App;
